#include <Arduino.h>
#include "SimpleTimer.h"

#define IN1_przod 4
#define IN2_przod 6
#define IN1_tyl 8
#define IN2_tyl 9
#define PWM_przod 3
#define PWM_tyl 7
#define Buzzer 11

#define echo_L 49
#define echo_R 51
#define echo_P 53
#define trig_L 48
#define trig_R 50
#define trig_P 52

#define kat A0
#define Czujnik_pradu A15

#define PinA 20
#define PinB 21

int delayval = 200;
void prosty_ruch_ustaw();
long duration;
long distance;
int unsigned zmiana_wartosci = 10;
char znak=0;
bool K=0;
int pwm=60;
bool Buzzer_V =0;

const byte numChars = 32;
char Wiadomosc[numChars];
bool newData = false;
unsigned short int rozmiar_wiadomosci = 0;
int moc_tyl=20;
int moc_przod=60;

void Przeslij_dane();
int odleglosc(int trigPin,int echoPin);
void wyslij(char * tresc);
void recvWithStartEndMarkers();
void Wykonaj_task();
void blinkA();
void blinkB();

void setup(){
    Serial.begin(115200);
    Serial.print("<X>");

    pinMode(IN1_przod, OUTPUT);
    pinMode(IN2_przod, OUTPUT);
    pinMode(IN1_tyl, OUTPUT);
    pinMode(IN2_tyl,OUTPUT);
    pinMode(PWM_przod,OUTPUT);
    pinMode(PWM_tyl,OUTPUT);
    pinMode(Buzzer,OUTPUT);

    pinMode(echo_L,INPUT);
    pinMode(echo_R,INPUT);
    pinMode(echo_P,INPUT);
    pinMode(trig_L,OUTPUT);
    pinMode(trig_R,OUTPUT);
    pinMode(trig_P,OUTPUT);

    pinMode(kat,INPUT_PULLUP);
    pinMode(Czujnik_pradu,INPUT_PULLUP);

    pinMode(PinA,INPUT_PULLUP);
    pinMode(PinB,INPUT_PULLUP);
    attachInterrupt(2, blinkA, RISING );
    attachInterrupt(3, blinkB, RISING );

    delay(100);
}
float dystans_przod = 0;
float dystans_tyl = 0;

volatile bool Stan_pinuA =0;
volatile bool Stan_pinuB =0;
volatile unsigned int obrot_przod =0;
volatile unsigned int obrot_tyl= 0;

void blinkA()
{
    if (Stan_pinuB==1){ //Przod
        Stan_pinuB=0;
        obrot_przod +=1;
    }
    else Stan_pinuA =1;
}

void blinkB()
{
    if (Stan_pinuA==1){ // Tyl
        Stan_pinuA =0;
        obrot_tyl+=1;
    }
    else Stan_pinuB= 1;
}

void recvWithStartEndMarkers(){
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;

    while (Serial.available() > 0 && newData == false){
        rc = Serial.read();

        if (recvInProgress == true){
            if (rc != endMarker){
                Wiadomosc[ndx] = rc;
                ndx++;
                if (ndx >= numChars)
                    ndx = numChars - 1;
            }
            else{
                Wiadomosc[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                newData = true;
                rozmiar_wiadomosci = ndx;
                ndx = 0;
            }
        }
        else if (rc == startMarker)
            recvInProgress = true;

    }
}
int wartosc_pradu;
int unsigned k1 =0;
int unsigned k2 =100;
int unsigned k3 =200;

int czujnik1V;
int czujnik2V;
int czujnik3V;
void loop(){
    recvWithStartEndMarkers();
    Wykonaj_task();
    k1++;k2++;k3++;
    dystans_przod = obrot_przod * 3 *0.5;  // czesciowy obrot kola * pi * srednica w cm/ ilosc potrzebnych do 1 obrotu
    dystans_tyl   = obrot_tyl * 3 *0.5;

    if (k1 ==300)
    {
    czujnik1V = odleglosc(trig_L,echo_L); k1=0;
    }
    if (k2 ==300)
    {
    czujnik2V = odleglosc(trig_R,echo_R); k2=0;
    }
    if (k3 ==300)
    {
    czujnik3V = odleglosc(trig_P,echo_P); k3=0;
    }

    //obrot_przod =0;
    // obrot_tyl= 0;

    wartosc_pradu =analogRead(Czujnik_pradu);

}

void Wykonaj_task(){

    if (newData == true){
        prosty_ruch_ustaw();
        newData = false;
    }
}



void Przeslij_dane(){

    int odl[3];
    odl[0] = czujnik1V;
    odl[1] = czujnik2V;
    odl[2] = czujnik3V;
    char * dupa = new char[15];

    Serial.print("<L");
    itoa(odl[1],dupa,10);
    Serial.print(dupa);

    Serial.print("R");
    itoa(odl[2],dupa,10);
    Serial.print(dupa);

    Serial.print("P");
    itoa(odl[0],dupa,10);
    Serial.print(dupa);

    Serial.print("K");
    itoa(analogRead(kat),dupa,10);
    Serial.print(dupa);

    Serial.print("W");
    itoa(wartosc_pradu,dupa,10);
    Serial.print(dupa);

    Serial.print("W");
    itoa(int(dystans_przod),dupa,10);
    Serial.print(dupa);

    Serial.print("W");
    itoa(int(dystans_tyl) ,dupa,10);
    Serial.print(dupa);

    Serial.print(">");
}
void Przeslij_dane_test(){

   Serial.println("Dane pradu:");
   Serial.println(wartosc_pradu);
   Serial.println("Odleglosci:");
   Serial.println("Przod:");
   Serial.println(obrot_przod);
   Serial.println("Tyl:");
   Serial.println(obrot_tyl);
}

bool czy_liczba(char x){
    if ( x>47 && x<58)  return true;
    else                return false;
}


void wyslij(char * tresc){
  Serial.print("<");
  Serial.print(tresc);
  Serial.print(">");
}


void prosty_ruch_ustaw()
{
    unsigned short int kursor = 0;

    /*
    for ( kursor; kursor<rozmiar_wiadomosci;kursor++)
        if (!czy_liczba(Wiadomosc[kursor]))
            break;

    char * liczba_c = new char[kursor -1];

    for ( kursor =1; kursor<rozmiar_wiadomosci;kursor++)
        if (!czy_liczba(Wiadomosc[kursor]))
            break;
        else
            liczba_c[kursor]= Wiadomosc[kursor];

    // atoi(liczba_c);
*/

    switch(Wiadomosc[0]){

    case 'a':  // lewo

        digitalWrite(IN1_przod, LOW);
        digitalWrite(IN2_przod, HIGH);
        analogWrite(PWM_przod,moc_przod);

        delay(delayval);
        digitalWrite(PWM_przod,0);
        wyslij(Wiadomosc);

        break;

    case 'd' : // prawo

        digitalWrite(IN1_przod, HIGH);
        digitalWrite(IN2_przod, LOW);
        analogWrite(PWM_przod,moc_przod);

        delay(delayval);
        digitalWrite(PWM_przod,0);
        wyslij(Wiadomosc);
        break;

    case 'w':  // Prosto
        digitalWrite(IN1_tyl, LOW);
        digitalWrite(IN2_tyl, HIGH);
        analogWrite(PWM_tyl,moc_tyl);
        wyslij(Wiadomosc);
        break;

    case 's':  // Do tylu
        digitalWrite(IN1_tyl, HIGH);
        digitalWrite(IN2_tyl, LOW);
        analogWrite(PWM_tyl,moc_tyl);
        wyslij(Wiadomosc);
        break;


    case 'p':
        moc_przod = atoi(Wiadomosc +1);
        wyslij(Wiadomosc);
        break;

    case 't':
        moc_tyl = atoi(&Wiadomosc[1]);
        wyslij(Wiadomosc);
        break;

    case 'x':

        digitalWrite(IN1_tyl, LOW);
        digitalWrite(IN2_tyl, LOW);
        digitalWrite(PWM_tyl,0);
        wyslij(Wiadomosc);
        break;

    case 'b':
        if (Buzzer_V){
            analogWrite(Buzzer, 0);
        }
        else{
            analogWrite(Buzzer, 125);
        }
        Buzzer_V = !Buzzer_V;
        wyslij(Wiadomosc);
        break;

    case 'm':
        Przeslij_dane();
        break;

    case 'y':
        Przeslij_dane_test();
        break;

    default :
             wyslij("E");
        break;

    }

    return;
}

unsigned long countPulseASM(volatile uint8_t *port, uint8_t bit, uint8_t stateMask, unsigned long maxloops)
 {
     unsigned long width = 0;
     // wait for any previous pulse to end
     while ((*port & bit) == stateMask)
         if (--maxloops == 0)
             return 0;

     // wait for the pulse to start
     while ((*port & bit) != stateMask)
         if (--maxloops == 0)
             return 0;

      // wait for the pulse to stop
      while ((*port & bit) == stateMask) {
          if (++width == maxloops)
              return 0;
      }
      return width;
 }

unsigned long pulseIn2(uint8_t pin, uint8_t state, unsigned long timeout)
{
	// cache the port and bit of the pin in order to speed up the
	// pulse width measuring loop and achieve finer resolution.  calling
	// digitalRead() instead yields much coarser resolution.
	uint8_t bit = digitalPinToBitMask(pin);
	uint8_t port = digitalPinToPort(pin);
	uint8_t stateMask = (state ? bit : 0);

	// convert the timeout from microseconds to a number of times through
	// the initial loop; it takes approximately 16 clock cycles per iteration
	unsigned long maxloops = microsecondsToClockCycles(timeout)/16;

	unsigned long width = countPulseASM(portInputRegister(port), bit, stateMask, maxloops);

	// prevent clockCyclesToMicroseconds to return bogus values if countPulseASM timed out
	if (width)
		return clockCyclesToMicroseconds(width * 16 + 16);
	else
		return 0;
}

int odleglosc(int trigPin,int echoPin)
{
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    duration = pulseIn2(echoPin, HIGH,5000);
    distance= duration*0.017;  // 0.034/2
    return distance;
}









