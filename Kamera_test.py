from picamera import PiCamera
from time import sleep

camera = PiCamera()
camera.resolution= (1280, 720)
camera.framerate = 5

camera.start_preview()
sleep(5)
camera.stop_preview()