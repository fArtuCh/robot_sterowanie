import PyQt5.QtWidgets as qq
import numpy as np
import random
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5 import QtTest
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
import threading
import re
import serial
import tkinter as tk
import sys
import qimage2ndarray
from PyQt5.QtCore import (QCoreApplication, QObject, QRunnable, QThread, QThreadPool, pyqtSignal)
import socket
import numpy as np
import math
from picamera.array import PiRGBArray
from picamera import PiCamera

width = 832
height = 832
camera = PiCamera()
camera.resolution = (width, height)
camera.framerate = 20
rawCapture = PiRGBArray(camera, size=(width, height))

root = tk.Tk()
Sw = root.winfo_screenwidth() - 50
Sh = root.winfo_screenheight() - 50
port_adres = '/dev/ttyACM0'
HOST = '192.168.0.116'
PORT = 8080

class Awysylanie(QThread):
    def __init__(self, uchwyt, port, szybkosc):
        super(Awysylanie, self).__init__()
        self.rodzic = uchwyt
        self.wysylaj = False
        self.arduino = serial.Serial(port, szybkosc, timeout=1)
        self.arduino.write(b'S')
        print(self.arduino.read(3))

    def run(self):
        while True:
            if self.wysylaj is True:

                try:
                    self.arduino.write(self.rodzic.wiadomosc_w)
                    #print("W:", self.rodzic.wiadomosc_w)

                    QtTest.QTest.qWait(30)

                    raw_message = b''
                    rozpoczeto_otrzymywanie = False

                    for i in range(4):
                        znacznik_startu = self.arduino.read(1)
                        #print(znacznik_startu)
                        if znacznik_startu == b'<':
                            rozpoczeto_otrzymywanie = True
                            break

                    otrzymany_bit = b''
                    if rozpoczeto_otrzymywanie is True:
                        while True:
                            raw_message += otrzymany_bit
                            otrzymany_bit = self.arduino.read(1)
                            #print("kolejka:", self.arduino.inWaiting(), "aktualny:", otrzymany_bit)
                            if self.arduino.inWaiting() == 0 or otrzymany_bit is b'>':
                                break

                    # remove EOL characters from string
                    message = raw_message.rstrip()

                    if raw_message == self.rodzic.wiadomosc_w:
                        self.rodzic.potwierdzenie = True

                    # Parser wiadomosci:

                    message = message.decode("utf-8")

                    self.rodzic.wiadomosc_o = message
                    if message[0] == "L":
                        x = re.split("[A-z]", message)
                        self.rodzic.C1V = int(x[1])
                        self.rodzic.C2V = int(x[2])
                        self.rodzic.C3V = int(x[3])
                        self.rodzic.C4V = int(x[4])

                        self.rodzic.C5V = int(x[5]) # prad silnika
                        self.rodzic.C6V = int(x[6]) # przod dystans
                        self.rodzic.C7V = int(x[7]) # tyl dystans

                        self.rodzic.C1w.setText(x[1])
                        self.rodzic.C2w.setText(x[2])
                        self.rodzic.C3w.setText(x[3])
                        self.rodzic.C4w.setText(x[4])

                        self.rodzic.C5w.setText(x[5])
                        self.rodzic.C6w.setText(x[6])
                        self.rodzic.C7w.setText(x[7])
                    else:
                        print("O:", message)

                
                except:
                    1

                self.wysylaj = False

            else:
                QtTest.QTest.qWait(50)


class Kamera(QThread):
    przekaz_obraz = pyqtSignal(np.ndarray)

    def __init__(self, uchwyt, obraz_klasa):
        super(Kamera, self).__init__()
        self.rodzic = uchwyt
        self.obraz = obraz_klasa

    def run(self):
        for frame in camera.capture_continuous(rawCapture, format="rgb", use_video_port=True):
            image = frame.array
            rawCapture.truncate(0)
            self.przekaz_obraz.emit(image)
            QtTest.QTest.qWait(20)


class Ikona(qq.QGraphicsItem):
    def __init__(self, pixmap, Wx, Wy, Wx2, Wy2):
        super(qq.QGraphicsItem, self).__init__()
        self.acceptHoverEvents()
        self.setAcceptHoverEvents(True)
        self.pixmap = QPixmap(pixmap)
        self.xQ = Wx
        self.yQ = Wy
        self.szerokosc = Wx2
        self.wysokosc = Wy2

    def paint(self, painter, QStyleOptionGraphicsItem, widget=None):
        painter.drawPixmap(int(self.xQ), int(self.yQ), int(self.szerokosc), int(self.wysokosc), self.pixmap)
        return

    def boundingRect(self):
        self.rectF = QRectF(self.xQ, self.yQ, self.szerokosc, self.wysokosc)
        return self.rectF


class Piece(qq.QGraphicsItem):

    def __init__(self, x, y, w, h):
        super(qq.QGraphicsItem, self).__init__()
        self.xQ = x
        self.yQ = y
        self.szerokosc = w
        self.wysokosc = h
        self.pixmap = QPixmap()
        self.rectF = QRectF(self.xQ, self.yQ, self.szerokosc, self.wysokosc)
        self.update(self.rectF)

    def paint(self, painter, QStyleOptionGraphicsItem, widget=None):
        painter.drawPixmap(int(self.xQ), int(self.yQ), int(self.szerokosc), int(self.wysokosc), self.pixmap)

        return

    def boundingRect(self):
        self.rectF = QRectF(self.xQ, self.yQ, self.szerokosc, self.wysokosc)
        return self.rectF

    def odswiez(self, pixmap):
            self.pixmap = pixmap
            self.xQ =-28
            self.yQ = -28
            self.szerokosc = self.pixmap.width()
            self.wysokosc = self.pixmap.height()
            self.prepareGeometryChange()
            self.update(self.rectF)
            return


class FPGA(QThread):

    rozmiar = width * height * 3

    def __init__(self, uchwyt):
        super(FPGA, self).__init__()
        self.rodzic = uchwyt
        self.odbieraj = True

    def int32(self, tab):
        return int(tab[0] << 24 ^ tab[1] << 16 ^ tab[2] << 8 ^ tab[3])

    def B8_16(self, liczba):
        return bytearray([liczba >> 8, liczba & 0xFF])

    def B32(self, liczba):
        return bytearray([liczba >> 24 & 0xFF, liczba >> 16 & 0xFF, liczba >> 8 & 0xFF, liczba & 0xFF])

    def run(self):
        
        rozmiar_logiki = 18
        rozmiar_buffora = 8192
        rozmiar_danych = rozmiar_buffora - rozmiar_logiki
        liczba_pakietow = math.ceil(self.rozmiar / rozmiar_danych)
        rozmiar = width * height * 3

        width_B = self.B8_16(width)
        height_B = self.B8_16(height)
        liczba_pakietow_B = self.B8_16(liczba_pakietow)
        while 1:
            QtTest.QTest.qWait(3500)
            if self.rodzic.zdj_aktualne is True:
                print("Nawiazuje polaczenie")

                # liczba_bitow              - 2 bajty liczba pakietow     - 2 bajty
                # nr pakietu             - 2 bajty miesjce_pamieci_start  - 4 bajty
                # miesjce_pamieci_koniec - 4 bajty zdjecie                - 1010 bajty
                
                try:
                    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                        s.connect((HOST, PORT))
                        while 1:

                            #dane = np.reshape(self.rodzic.zdj_buf, (1, rozmiar)).tolist()
                            #dane = bytearray(dane[0])
                            dane = self.rodzic.zdj_buf.tobytes()
                            s.sendall(bytearray('R', 'utf-8'))
                            data = s.recv(1)
                            wiadomosc = data.decode("utf-8")
                            if len(wiadomosc) > 0 and wiadomosc[0] != "R":
                                continue
                            print("Wysylam_obraz")
                            for i in range(liczba_pakietow):
                                pamiec_start = i * rozmiar_danych
                                pamiec_koniec = pamiec_start + rozmiar_danych
                                if pamiec_koniec > rozmiar:
                                    pamiec_koniec = rozmiar

                                i_B = self.B8_16(i)
                                pam_st_B = self.B32(pamiec_start)
                                pam_kn_B = self.B32(pamiec_koniec)
                                liczba_B = self.B8_16(18 + pamiec_koniec - pamiec_start)

                                BUFFOR_wys = liczba_B + width_B + height_B + liczba_pakietow_B+i_B+pam_st_B+pam_kn_B+dane[pamiec_start:pamiec_koniec]

                                s.sendall(BUFFOR_wys)

                            print("Koniec przesylania")
                            data = s.recv(1)
                            wiadomosc = data.decode("utf-8")
                            if len(wiadomosc) > 0 and wiadomosc[0] != "K":
                                continue

                            data = s.recv(21)
                            x = list(data)
                            if len(x) > 0 and (int(x[0]) != 87 and int(x[0]) != 78):
                                continue
                            if int(x[0]) == 87:
                                srodekx = self.int32(x[1:5])
                                srodeky = self.int32(x[5:9])
                                szerokosc = self.int32(x[9:13])
                                wysokosc = self.int32(x[13:17])
                                procent = self.int32(x[17:21])
                                print('Data: srodek=(', srodekx, ",", srodeky, ") ", "wymiary=(", szerokosc, ",",
                                      wysokosc, ") procent= ", procent)
                                self.rodzic.ramka = [srodekx,srodeky,szerokosc,wysokosc]

                            else:
                                self.rodzic.ramka = []
                                print("Nie znaleziono obiektu")
                except Exception as e:
                    continue


class Stream(QObject):
    newText = pyqtSignal(str)

    def write(self, text):
        self.newText.emit(str(text))

class Switcher(object):
    etap_obrotu = 0

    def __init__(self,uchwyt):
        self.rodzic = uchwyt

    def nr_to_obrot(self, argument):
        """Dispatch method"""
        method_name = 'NrO_' + str(argument)
        # Get the method from 'self'. Default to a lambda.
        method = getattr(self, method_name)
        # Call the method as we return it
        return method()

    def wykonaj(self):
        method_name = 'NrO_' + str(self.etap_obrotu)
        # Get the method from 'self'. Default to a lambda.
        method = getattr(self, method_name)
        # Call the method as we return it
        self.etap_obrotu += 1
        if self.etap_obrotu > 3:
            self.etap_obrotu = 0

        return method()

    def NrO_0(self):
        #Kola na lewo
        self.rodzic.do_lewej()
        QtTest.QTest.qWait(1000)

    def NrO_1(self):
        self.rodzic.do_przodu()
        QtTest.QTest.qWait(3000)
        self.rodzic.zatrzymaj()

    def NrO_2(self):
        self.rodzic.do_prawej()
        QtTest.QTest.qWait(1000)

    def NrO_3(self):
        self.rodzic.do_tylu()
        QtTest.QTest.qWait(3000)
        self.rodzic.zatrzymaj()


class Window(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)
        self.resize(Sw, Sh)

        self.threadtest = QThread(self)
        self.idealthreadcount = self.threadtest.idealThreadCount()
        print("This machine can handle {} threads optimally".format(self.idealthreadcount))

        self.P_dol = self.przycisk([Sw * 0.06, Sh * 0.9, Sw * 0.05, Sh * 0.05], 15, "S", self)
        self.P_gora = self.przycisk([Sw * 0.06, Sh * 0.8, Sw * 0.05, Sh * 0.05], 15, "W", self)
        self.P_lewo = self.przycisk([Sw * 0.01, Sh * 0.85, Sw * 0.05, Sh * 0.05], 15, "A", self)
        self.P_prawo = self.przycisk([Sw * 0.11, Sh * 0.85, Sw * 0.05, Sh * 0.05], 15, "D", self)
        self.Stoj = self.przycisk([Sw * 0.06, Sh * 0.85, Sw * 0.05, Sh * 0.05], 15, "C", self)
        self.Moc_przod = self.przycisk([Sw * 0.16, Sh * 0.85, Sw * 0.05, Sh * 0.05], 15, "P+", self)
        self.Moc_tyl = self.przycisk([Sw * 0.16, Sh * 0.9, Sw * 0.05, Sh * 0.05], 15, "T+", self)
        self.Dane = self.przycisk([Sw * 0.0, Sh * 0.95, Sw * 0.21, Sh * 0.05], 15, "Pobieranie danych", self)
        self.Auto = self.przycisk([Sw * 0.65, Sh * 0.65, Sw * 0.07, Sh * 0.07], 20, "Auto", self)
        self.Clr = self.przycisk([Sw * 0.65, Sh * 0.72, Sw * 0.07, Sh * 0.07], 20, "Clr", self)
        self.IP = self.przycisk([Sw * 0.65, Sh * 0.79, Sw * 0.07, Sh * 0.07], 20, "IP", self)
        self.C1V = 0; self.C2V = 0 ; self.C3V = 0 ; self.C4V = 0; self.C5V = 0 ; self.C6V = 0 ; self.C7V = 0

        self.C1 = self.label([Sw * 0.3, Sh * 0.8, Sw * 0.1, Sh * 0.1], 15, "UL:", self)
        self.C2 = self.label([Sw * 0.3, Sh * 0.84, Sw * 0.1, Sh * 0.1], 15, "UR:", self)
        self.C3 = self.label([Sw * 0.3, Sh * 0.88, Sw * 0.1, Sh * 0.1], 15, "US:", self)
        self.C4 = self.label([Sw * 0.3, Sh * 0.92, Sw * 0.1, Sh * 0.1], 15, "A:", self)
        self.C5 = self.label([Sw * 0.4, Sh * 0.8, Sw * 0.1, Sh * 0.1], 15, "Prad S:", self)
        self.C6 = self.label([Sw * 0.4, Sh * 0.84, Sw * 0.1, Sh * 0.1], 15, "Dyst. P:", self)
        self.C7 = self.label([Sw * 0.4, Sh * 0.88, Sw * 0.1, Sh * 0.1], 15, "Dyst. Wst:", self)
        self.C1w = self.label([Sw * 0.33, Sh * 0.8, Sw * 0.1, Sh * 0.1], 15, str (self.C1V), self)
        self.C2w = self.label([Sw * 0.33, Sh * 0.84, Sw * 0.1, Sh * 0.1], 15, str(self.C2V), self)
        self.C3w = self.label([Sw * 0.33, Sh * 0.88, Sw * 0.1, Sh * 0.1], 15, str(self.C3V), self)
        self.C4w = self.label([Sw * 0.33, Sh * 0.92, Sw * 0.1, Sh * 0.1], 15, str(self.C4V), self)
        self.C5w = self.label([Sw * 0.48, Sh * 0.8, Sw * 0.1, Sh * 0.1], 15, str(self.C5V), self)
        self.C6w = self.label([Sw * 0.48, Sh * 0.84, Sw * 0.1, Sh * 0.1], 15, str(self.C6V), self)
        self.C7w = self.label([Sw * 0.48, Sh * 0.88, Sw * 0.1, Sh * 0.1], 15, str(self.C7V), self)
        #self.Lobiekt = self.label([Sw * 0.4, Sh * 0.82, Sw * 0.1, Sh * 0.1], 15, "Pozycja:", self)
        self.LAuto_stan = self.label([Sw * 0.73, Sh * 0.63, Sw * 0.1, Sh * 0.1], 20, "OFF", self)
        self.LAuto_stan.setStyleSheet('color: red')
        self.Auto_stan = False

        self.consola = qq.QTextEdit(self)
        self.consola.moveCursor(QTextCursor.Start)
        self.consola.ensureCursorVisible()
        self.consola.setGeometry(QRect(Sw * 0.65, Sh * 0.0 , Sw * 0.35, Sh * 0.65))
        font = QFont()
        font.setPointSize(16)
        self.consola.setFont(font)
        self.consola.setStyleSheet('background-color: #03090A; color: green')
        self.consola.setText("Console data: > Status on \n")
        self.consola.show()
        self.ramka = []
        self.EMocPrzod = qq.QTextEdit(self)
        self.EMocPrzod.setGeometry(QRect(Sw * 0.21, Sh * 0.85, Sw * 0.05, Sh * 0.05))
        self.EMocPrzod.setText("30"); self.EMocPrzod.show()

        self.EMocTyl = qq.QTextEdit(self)
        self.EMocTyl.setGeometry(QRect(Sw * 0.21, Sh * 0.9, Sw * 0.05, Sh * 0.05))
        self.EMocTyl.setText("30"); self.EMocTyl.show()

        self.IP_zmien = qq.QTextEdit(self)
        self.EMocTyl.setGeometry(QRect(Sw * 0.73, Sh * 0.79, Sw * 0.07, Sh * 0.07))
        self.EMocTyl.setText(HOST);
        self.EMocTyl.show()

        self.okienko_na_obraz = qq.QGraphicsView(self)
        self.okienko_na_obraz.setGeometry(Sw * 0.0, Sh * 0.0, Sw * 0.65, Sh * 0.8)
        rcontent = self.okienko_na_obraz.contentsRect();
        self.scene = qq.QGraphicsScene(self)
        self.scene.setSceneRect(0, 0, rcontent.width(), rcontent.height());
        self.scene.clear()
        self.okienko_na_obraz.setScene(self.scene)

        self.graficzka = Piece(Sw * 0.0, Sh * 0.0, Sw * 0.59, Sh * 0.79)
        #self.scene.addItem(Ikona(QPixmap("Samochod.png"), Sw * 0.3, Sh * 0.3, Sw * 0.05, Sh * 0.1))
        self.scene.addItem(self.graficzka)

        self.wiadomosc_w = b'<w>'
        self.wiadomosc_o = b'<w>'
        self.potwierdzenie = False
        self.zdj_aktualne = False
        self.obiekt_wykryty = False
        self.Pole_obiektu = 0
        self.Pozycja_obiektu = [0,0]

        self.P_gora.clicked.connect(self.do_przodu)
        self.P_dol.clicked.connect(self.do_tylu)
        self.P_lewo.clicked.connect(self.do_lewej)
        self.P_prawo.clicked.connect(self.do_prawej)
        self.Stoj.clicked.connect(self.zatrzymaj)
        self.Dane.clicked.connect(self.daj_dane)
        self.Auto.clicked.connect(self.zmiana_trybu)
        self.Clr.clicked.connect(self.czysc)
        self.Moc_przod.clicked.connect(self.zmiana_mocy_przod)
        self.Moc_tyl.clicked.connect(self.zmiana_mocy_tyl)
        self.IP.clicked.connect(self.zmiana_ip)

        sys.stdout = Stream(newText=self.onUpdateText)

        self.thread = Awysylanie(self, port_adres, 115200)
        self.thread.start()

        self.thread_socket = FPGA(self)
        self.thread_socket.start()

        self.przechwyt_kamery = Kamera(self, self.graficzka)
        self.przechwyt_kamery.przekaz_obraz.connect(self.updateobraz)
        self.przechwyt_kamery.start()

        th = threading.Thread(target=self.watek_obslugujacy, args=())
        th.start()
        th2 = threading.Thread(target=self.pobieranie_informacji, args=())
        th2.start()

    def zmiana_ip(self):
        HOST = self.IP_zmien.toPlainText().strip()
        print("Zmieniono docelowe IP")

    def czysc(self):
        self.consola.setText(" ")

    @pyqtSlot(np.ndarray)
    def updateobraz(self, nparray):
        self.zdj_aktualne = True
        self.zdj_buf = nparray.copy()
        if len(self.ramka) > 0:
            self.obiekt_wykryty = True
            W_srx = self.ramka[0]
            W_sry = self.ramka[1]
            Szer = self.ramka[2]
            Wys = self.ramka[3]
            self.Pole_obiektu = Szer * Wys
            self.Pozycja_obiektu = [W_srx, W_sry]

            pkt_l_G = [int(W_srx - Szer/2), int(W_sry - Wys/2)]
            pkt_l_D = [int(W_srx - Szer/2), int(W_sry + Wys/2)]
            pkt_P_G = [int(W_srx + Szer/2), int(W_sry - Wys/2)]
            pkt_P_D = [int(W_srx + Szer/2), int(W_sry + Wys/2)]

            self.dorysuj_linie(pkt_l_G, pkt_l_D, self.zdj_buf)
            self.dorysuj_linie(pkt_P_G, pkt_P_D, self.zdj_buf)

            self.dorysuj_linie(pkt_l_G, pkt_P_G, self.zdj_buf)
            self.dorysuj_linie(pkt_l_D, pkt_P_D, self.zdj_buf)
        else :
            self.obiekt_wykryty = False

        self.pix_map = QPixmap.fromImage(qimage2ndarray.array2qimage(self.zdj_buf))
        self.graficzka.odswiez(self.pix_map)

    def sprawdz_punkt(self, Px, kraniec=832):
        if Px[0] < 1:
            Px[0] = 1
        if Px[0] > kraniec:
            Px[0] = 831
        if Px[1] < 1:
            Px[1] = 1
        if Px[1] > kraniec:
            Px[1] = 831
        return Px


    def dorysuj_linie(self, P1, P2, array):

        P1 = self.sprawdz_punkt(P1)
        P2 = self.sprawdz_punkt(P2)
        X = int (P2[0] - P1[0])
        Y = int (P2[1] - P1[1])

        for i in range(abs(X)):
            temp_i = i
            if X < 0:
                temp_i *= -1

            array[P1[1]][P1[0] + temp_i][0] = 255
            array[P1[1]][P1[0] + temp_i][1] = 0
            array[P1[1]][P1[0] + temp_i ][2] = 255

        for k in range(abs(Y)):
            temp_k = k
            if Y < 0:
                temp_k *= -1

            array[P1[1] + temp_k][P1[0] ][0] = 255
            array[P1[1] + temp_k][P1[0] ][1] = 0
            array[P1[1] + temp_k][P1[0] ][2] = 255

        return

    def onUpdateText(self, text):
        cursor = self.consola.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertText(text)
        self.consola.setTextCursor(cursor)
        self.consola.ensureCursorVisible()

    def watek_obslugujacy(self):

        kontroler = Switcher(self)
        while True:
            QtTest.QTest.qWait(20)

            if self.Auto_stan is True:
                if self.obiekt_wykryty is False:
                    kontroler.wykonaj()  # Obrot 0 ->1 -> 2 ->3
                else:
                    self.Zatrzymaj()
                    self.buzzer()
                    QtTest.QTest.qWait(1000)
                    self.buzzer()
                    self.zmiana_trybu()
                    kontroler.etap_obrotu = 0

                    QtTest.QTest.qWait(100)
                    if self.Pozycja_obiektu[0] >530:
                        self.do_prawej()
                        QtTest.QTest.qWait(400)
                        self.do_przodu()
                        QtTest.QTest.qWait(3000)
                    elif self.Pozycja_obiektu[0] <330:
                        self.do_lewej()
                        QtTest.QTest.qWait(400)
                        self.do_przodu()
                        QtTest.QTest.qWait(3000)
                    else:
                        self.do_przodu()
                        QtTest.QTest.qWait(1000)
                    self.Zatrzymaj()

            else:
                QtTest.QTest.qWait(50)
                #self.Zatrzymaj()

    def pobieranie_informacji(self):

        while True:
                QtTest.QTest.qWait(300)
                self.daj_dane()
                if (self.C1V >0 and self.C1V<20) or (self.C2V >0 and self.C2V<20) or (self.C2V >0 and self.C2V<20):
                    if self.Auto_stan:
                        self.zmiana_trybu()
                    self.zatrzymaj()
                    QtTest.QTest.qWait(4000)

    def Odswiez(self):
        1

    def zmiana_trybu(self):
        self.Auto_stan = not self.Auto_stan
        if self.Auto_stan:
            self.LAuto_stan.setText("ON")
            self.LAuto_stan.setStyleSheet('color: green')
        else:
            self.LAuto_stan.setText("OFF")
            self.LAuto_stan.setStyleSheet('color: red')

    def do_przodu(self):
        self.wyslij_do_arduino( b'<w>')

    def do_tylu(self):
        self.wyslij_do_arduino(b'<s>')

    def do_lewej(self):
        self.wyslij_do_arduino(b'<a>')

    def do_prawej(self):
        self.wyslij_do_arduino(b'<d>')

    def daj_dane(self):
        self.wyslij_do_arduino(b'<m>')

    def zatrzymaj(self):
        self.wyslij_do_arduino( b'<x>')

    def buzzer(self):
        self.wyslij_do_arduino( b'<b>')

    def zmiana_mocy_tyl(self):
        self.wyslij_do_arduino(b'<p20>')

    def zmiana_mocy_przod(self):
        self.wyslij_do_arduino(b'<t20>')

    def wyslij_do_arduino(self, symbol):
        while self.thread.wysylaj is True:
            QtTest.QTest.qWait(40)
        self.wiadomosc_w = symbol
        self.thread.wysylaj = True

    def przycisk(self, Wymiar, Czcionka, Nazwa, Uchwyt):
        Przycisk = qq.QPushButton(Uchwyt)
        Przycisk.setGeometry(QRect(Wymiar[0], Wymiar[1], Wymiar[2], Wymiar[3]))
        font = QFont()
        font.setPointSize(Czcionka)
        Przycisk.setFont(font)
        Przycisk.setObjectName(Nazwa)
        Przycisk.setText(Nazwa)
        return Przycisk

    def label(self, Wymiar, Czcionka, Nazwa, Uchwyt):
        Przycisk = qq.QLabel(Uchwyt)
        Przycisk.setGeometry(QRect(Wymiar[0], Wymiar[1], Wymiar[2], Wymiar[3]))
        # Przycisk.setStyleSheet("background-color: #AAAAAA; border-radius: 30px;")
        font = QFont()
        font.setPointSize(Czcionka)
        Przycisk.setFont(font)
        Przycisk.setObjectName(Nazwa)
        Przycisk.setText(Nazwa)
        Przycisk.show()
        return Przycisk

    def __del__(self):
        sys.stdout = sys.__stdout__


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = Window()
    p = QPalette()
    gradient = QLinearGradient(0, 0, 0, 800)
    gradient.setColorAt(0.0, QColor(0, 210, 190))
    gradient.setColorAt(1.0, QColor(0, 102, 200))
    p.setBrush(QPalette.Window, QBrush(gradient))
    main.setPalette(p)
    main.setWindowTitle("Program zarzadzajacy")
    #main.showFullScreen()
    main.show()
    sys.exit(app.exec_())
